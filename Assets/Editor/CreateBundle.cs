﻿using UnityEngine;
using UnityEditor;
using System.IO;

public class CreateBundle : MonoBehaviour
{
    [MenuItem("Bundles/Build Asset Bundles")]
    static void Build()
    {
        string outputPath = EditorUtility.SaveFolderPanel("Save Bundle", "Resources/Assets/Bundles", ""); //сохранить папку
        if (!Directory.Exists(outputPath))
            Directory.CreateDirectory(outputPath);
        BuildPipeline.BuildAssetBundles(outputPath, BuildAssetBundleOptions.None, EditorUserBuildSettings.activeBuildTarget);
    }
}
