﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class AssetBundleLoader<T> : ILoader<T> where T : UnityEngine.Object
{
    public event Action<T> OnLoadComplete;

    public void Load(string _name, string _url)
    {
        var myLoadedAssetBundle = AssetBundle.LoadFromFile(Path.Combine(Application.streamingAssetsPath, "myassetBundle"));
        if (myLoadedAssetBundle == null)
        {
            Debug.Log("Failed to load AssetBundle!");
            return;
        }

        var prefab = myLoadedAssetBundle.LoadAsset<GameObject>("MyObject");
        Instantiate(prefab);

        myLoadedAssetBundle.Unload(false);

        //StartCoroutine(DownloadBundle(_name, _url));
    }

    IEnumerator DownloadBundle(string _name, string _url)
    {
        // Start a download of the given URL
        using (UnityWebRequest uwr = UnityWebRequest.GetAssetBundle(_url))
        {
            // Wait for download to complete
            yield return uwr.SendWebRequest();
            if (uwr.isNetworkError || uwr.isHttpError)
            {
                Debug.LogError(uwr.error);
            }
            else
            {
                // Get downloaded asset bundle
                AssetBundle bundle = DownloadHandlerAssetBundle.GetContent(uwr);
                // Load the TextAsset object
                T txt = bundle.LoadAsset<T>(_name);
                bundle.Unload(false);
                OnLoadComplete?.Invoke(txt);
            }
        }
    }
}
