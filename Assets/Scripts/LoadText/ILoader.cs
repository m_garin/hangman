﻿using System;

public interface ILoader<T>  {

    void Load(string _path, string _name);

    event Action<T> OnLoadComplete;
}
